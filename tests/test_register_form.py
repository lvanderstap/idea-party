import pytest

from django.contrib.auth.models import User

from idea_party.auth.forms import RegisterForm


@pytest.mark.django_db
class TestRegisterForm(object):
    @pytest.fixture(autouse=True)
    def init_data(self):
        User.objects.create_user(username='bestusername',
                                 email='bestusername@ham.com',
                                 password='pass')

    def test_existing_user_found(self):
        form = RegisterForm({'username': 'bestusername', 'password': '123', 'email': '123@ham.com'})
        assert not form.is_valid()

    def test_username_is_case_sensitive(self):
        # It is ok to have same username with different capitalization
        form = RegisterForm({'username': 'Bestusername', 'password': '123', 'email': '123@ham.com'})
        assert form.is_valid()

    def test_empty_string_username(self):
        form = RegisterForm({'username': '', 'password': '123', 'email': '123@ham.com'})
        assert not form.is_valid()

    def test_null_username(self):
        form = RegisterForm({'username': None, 'password': '123', 'email': '123@ham.com'})
        assert not form.is_valid()

    def test_null_string_username(self):
        form = RegisterForm({'username': 'NULL', 'password': '123', 'email': '123@ham.com'})
        assert form.is_valid()

    def test_empty_string_password(self):
        form = RegisterForm({'username': 'Bestusername', 'password': '', 'email': '123@ham.com'})
        assert not form.is_valid()

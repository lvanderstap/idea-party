from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^new/$', views.idea_new, name="idea_new"),
    url(r'^idea/(?P<pk>[0-9]+)/$', views.idea_detail, name='idea_detail'),
    url(r'^idea/(?P<pk>[0-9]+)/edit/', views.idea_edit, name='idea_edit'),

]
